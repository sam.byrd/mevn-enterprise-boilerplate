const request = require("supertest");
const path = require("path");
const app = require(path.join(__dirname, "../../../server"));
const { seedItems, populateItems } = require("./seed");
const mongoose = require("mongoose");

beforeEach(populateItems);

afterAll(async () => {
  await mongoose.connection.close();
});

describe("/items endpoints", () => {
  it("GET - should get all items", async () => {
    const res = await request(app)
      .get("/items")
      .expect(200);
    expect(res.body.length).toBe(seedItems.length);
  });

  it("POST - should add an item", async() => {
    const newItems = [
      {
        title: "new item 1"
      },
      {
        title: "new itme 2"
      }
    ];

    const res = await request(app)
      .post("/items")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .send(newItems)
      .expect(201);

    const res2 = await request(app)
      .get("/items")
      .expect(200);
      expect(res2.body.length).toBe(seedItems.length + 2);
  });
});
