![pipeline status](https://gitlab.com/sam.byrd/mevn-enterprise-boilerplate/badges/master/pipeline.svg)

# mevn-enterprise-boilerplate

The code for [Vue.js Enterprise Development Crash Course](https://crash-course.enterprisevue.com/).

## Prerequisites
```
MongoDB server installed and running
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit:client
npm run test:unit:server
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

## Deployment

- Create a new Heroku app
- In Gitlab UI go to *Settings > CI > Variables* and set `HEROKU_APP_NAME` and `HEROKU_API_KEY`.

## Additional functionality

- modified service to include ability to add an item via a POST
- Created Items page

## Future development

- wire up Items page to services to retrieve Items and Add services