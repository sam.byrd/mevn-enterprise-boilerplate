const bodyParser = require('body-parser');
const express = require("express");
const router = express.Router();
const ItemController = require("../controllers/items");

router.use(bodyParser.json());

router.route("/").get(ItemController.get);

router.route("/").post(ItemController.post);

module.exports = router;
